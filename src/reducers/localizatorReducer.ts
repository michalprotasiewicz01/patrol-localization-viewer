import { updateObject } from 'helper/objectUtils';
import localizatorActionTypes from 'constants/localizatorActionTypes';
import {
  ILocalizationState,
  IFetchLocalizationsDataSuccess,
  IFetchLocalizationsDataFailure,
  LocalizatorAllActionsTypes,
} from 'models/localizatorRedux';

const initialState = {
  data: [],
  isLoading: false,
  error: null,
};

const fetchLocalizationsDataRequest = (state: ILocalizationState): ILocalizationState =>
  updateObject(state, {
    data: state.data,
    isLoading: true,
    error: null,
  });

const fetchLocalizationsDataSuccess = (
  state: ILocalizationState,
  action: IFetchLocalizationsDataSuccess,
): ILocalizationState =>
  updateObject(state, {
    data: action.data,
    isLoading: false,
    error: null,
  });

const fetchLocalizationsDataFailure = (
  state: ILocalizationState,
  action: IFetchLocalizationsDataFailure,
): ILocalizationState =>
  updateObject(state, {
    data: state.data,
    isLoading: false,
    error: action.error,
  });

const localizatorReducer = (
  state: ILocalizationState = initialState,
  action: LocalizatorAllActionsTypes,
): ILocalizationState => {
  switch (action.type) {
    case localizatorActionTypes.FETCH_LOCALIZATIONS_DATA_REQUEST:
      return fetchLocalizationsDataRequest(state);
    case localizatorActionTypes.FETCH_LOCALIZATIONS_DATA_SUCCESS:
      return fetchLocalizationsDataSuccess(state, action);
    case localizatorActionTypes.FETCH_LOCALIZATIONS_DATA_FAILURE:
      return fetchLocalizationsDataFailure(state, action);
    default:
      return state;
  }
};

export default localizatorReducer;
