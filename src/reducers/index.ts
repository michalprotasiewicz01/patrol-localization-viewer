import { combineReducers } from 'redux';
import localizatorReducer from './localizatorReducer';

const rootReducer = combineReducers({
  localizator: localizatorReducer,
});

export default rootReducer;
