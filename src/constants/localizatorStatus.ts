const localizatorStatus = {
  alarm: 'Alarm',
  noAlarm: 'No alarm',
  offline: 'Device offline',
  unavailable: 'Device unavailable',
};

export default localizatorStatus;
