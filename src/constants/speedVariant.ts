const speedVariant = {
  walk: { text: 'walk', min: 0, max: 15 },
  bike: { text: 'bike', min: 10, max: 50 },
  car: { text: 'car', min: 30, max: 500 },
};

export default speedVariant;
