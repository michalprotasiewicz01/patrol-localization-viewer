import axios from 'axios';
import { Dispatch } from 'redux';
import localizatorActionTypes from 'constants/localizatorActionTypes';
import { LOCALIZATOR_DATA } from 'constants/apiRoutes';
import {
  IFetchLocalizationsDataRequest,
  IFetchLocalizationsDataSuccess,
  IFetchLocalizationsDataFailure,
  LocalizatorAllActionsTypes,
  ILocalizationObject,
} from 'models/localizatorRedux';

export const fetchLocalizationsDataRequest = (): IFetchLocalizationsDataRequest => ({
  type: localizatorActionTypes.FETCH_LOCALIZATIONS_DATA_REQUEST,
});

export const fetchLocalizationsDataSuccess = (
  data: ILocalizationObject[],
): IFetchLocalizationsDataSuccess => ({
  type: localizatorActionTypes.FETCH_LOCALIZATIONS_DATA_SUCCESS,
  data,
});

export const fetchLocalizationsDataFailure = (error: unknown): IFetchLocalizationsDataFailure => ({
  type: localizatorActionTypes.FETCH_LOCALIZATIONS_DATA_FAILURE,
  error,
});

export const fetchLocalizationsData = () => (dispatch: Dispatch<LocalizatorAllActionsTypes>) => {
  dispatch(fetchLocalizationsDataRequest());

  return axios
    .get(LOCALIZATOR_DATA)
    .then((payload) => {
      dispatch(fetchLocalizationsDataSuccess(payload.data));
    })
    .catch((error) => {
      dispatch(fetchLocalizationsDataFailure(error));
    });
};
