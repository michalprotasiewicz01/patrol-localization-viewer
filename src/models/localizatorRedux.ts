import { Action } from 'redux';
import localizatorActionType from 'constants/localizatorActionTypes';
import { IMainModel } from './sharedRedux';

export interface ILocalizationObject {
  id: string;
  sim_number: string;
  imei: string;
  last_latitude: string;
  last_longitude: string;
  last_altitude: string;
  last_speed: string;
  last_status: string;
  last_track_time: Date;
  last_heartbeat: string;
}

export interface ILocalizationState extends IMainModel {
  data: ILocalizationObject[] | [];
}

export interface IFetchLocalizationsDataRequest extends Action {
  type: localizatorActionType.FETCH_LOCALIZATIONS_DATA_REQUEST;
}

export interface IFetchLocalizationsDataSuccess extends Action {
  type: localizatorActionType.FETCH_LOCALIZATIONS_DATA_SUCCESS;
  data: ILocalizationObject[];
}

export interface IFetchLocalizationsDataFailure extends Action {
  type: localizatorActionType.FETCH_LOCALIZATIONS_DATA_FAILURE;
  error: unknown;
}

export type LocalizatorAllActionsTypes =
  | IFetchLocalizationsDataRequest
  | IFetchLocalizationsDataSuccess
  | IFetchLocalizationsDataFailure;
