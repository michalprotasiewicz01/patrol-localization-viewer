export interface IMainModel {
  isLoading: boolean;
  error: unknown | null;
}
