export const updateObject = <Type, UpdateType>(
  oldObject: Type,
  updatedProperties: UpdateType,
): Type => ({
  ...oldObject,
  ...updatedProperties,
});
