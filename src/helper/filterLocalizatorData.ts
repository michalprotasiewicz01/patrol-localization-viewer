import speedVariant from 'constants/speedVariant';
import localizatorStatus from 'constants/localizatorStatus';
import { ILocalizationObject } from 'models/localizatorRedux';
import { FilterValuesType } from 'components/FilterForm/FilterForm';

const filterLocalizatorData = (
  filteredObject: FilterValuesType,
  localizatorData: ILocalizationObject[],
): ILocalizationObject[] => {
  let filteredData = localizatorData;

  if (filteredObject.phoneNumber !== '') {
    filteredData = filteredData.filter((location) =>
      location.sim_number.includes(filteredObject.phoneNumber),
    );
  }

  if (filteredObject.imei !== '') {
    filteredData = filteredData.filter((location) => location.imei.includes(filteredObject.imei));
  }

  if (!filteredObject.speedVariant.some((variant) => variant === speedVariant.walk.text)) {
    filteredData = filteredData.filter(
      (location) =>
        !(
          Number(location.last_speed) >= speedVariant.walk.min &&
          Number(location.last_speed) <= speedVariant.walk.max
        ),
    );
  }

  if (!filteredObject.speedVariant.some((variant) => variant === speedVariant.bike.text)) {
    filteredData = filteredData.filter(
      (location) =>
        !(
          Number(location.last_speed) >= speedVariant.bike.min &&
          Number(location.last_speed) <= speedVariant.bike.max
        ),
    );
  }

  if (!filteredObject.speedVariant.some((variant) => variant === speedVariant.car.text)) {
    filteredData = filteredData.filter(
      (location) =>
        !(
          Number(location.last_speed) >= speedVariant.car.min &&
          Number(location.last_speed) <= speedVariant.car.max
        ),
    );
  }

  if (!filteredObject.statuses.noAlarm) {
    filteredData = filteredData.filter(
      (location) => location.last_status !== localizatorStatus.noAlarm,
    );
  }

  if (!filteredObject.statuses.alarm) {
    filteredData = filteredData.filter(
      (location) => location.last_status !== localizatorStatus.alarm,
    );
  }

  if (!filteredObject.statuses.offline) {
    filteredData = filteredData.filter(
      (location) => location.last_status !== localizatorStatus.offline,
    );
  }

  if (!filteredObject.statuses.unavailable) {
    filteredData = filteredData.filter(
      (location) => location.last_status !== localizatorStatus.unavailable,
    );
  }

  return filteredData;
};

export default filterLocalizatorData;
