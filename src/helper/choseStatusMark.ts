import localizatorStatus from 'constants/localizatorStatus';
import blueMark from 'assets/blue-mark.png';
import redMark from 'assets/red-mark.png';
import greenMark from 'assets/green-mark.png';
import grayMark from 'assets/gray-mark.png';

const choseStatusMark = (status: string): string => {
  switch (true) {
    case status === localizatorStatus.unavailable:
      return blueMark;
    case status === localizatorStatus.alarm:
      return redMark;
    case status === localizatorStatus.noAlarm:
      return greenMark;
    case status === localizatorStatus.offline:
      return grayMark;
    default:
      return blueMark;
  }
};

export default choseStatusMark;
