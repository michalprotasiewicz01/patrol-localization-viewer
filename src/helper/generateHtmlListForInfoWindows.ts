const generateHtmlListForInfoWindows = (
  title: string,
  listOfTexts: { label: string; value: string }[],
): string =>
  `<div><h3>${title}</h3><ul>${listOfTexts
    .map(({ label, value }) => `<li><b>${label}:</b> ${value}</li>`)
    .join('')}</ul></div>`;

export default generateHtmlListForInfoWindows;
