import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { Wrapper } from '@googlemaps/react-wrapper';
import { fetchLocalizationsData as fetchLocalizationsDataAction } from 'actions/localizator';
import {
  ILocalizationObject,
  ILocalizationState,
  LocalizatorAllActionsTypes,
} from 'models/localizatorRedux';
import useInterval from 'hooks/useInterval';
import filterLocalizatorData from 'helper/filterLocalizatorData';
import speedVariant from 'constants/speedVariant';
import { center, zoom } from 'constants/mapDefaultSettings';
import Map from 'components/Map/Map';
import Markers from 'components/Markers/Markers';
import FilterModal from 'components/FilterModal/FilterModal';
import FilterForm, { FilterValuesType } from 'components/FilterForm/FilterForm';

interface ILocalizator {
  localizator: ILocalizationState;
}

type LocalizatorProps = ConnectedProps<typeof connector>;

const Localizator = ({ localizator, fetchLocalizationsData }: LocalizatorProps): JSX.Element => {
  const [filteredLocalizatorData, setFilteredLocalizatorData] = useState<ILocalizationObject[]>(
    localizator.data,
  );
  const [filterParams, setFilterParams] = useState<FilterValuesType>({
    phoneNumber: '',
    imei: '',
    speedVariant: [speedVariant.walk.text, speedVariant.bike.text, speedVariant.car.text],
    statuses: { noAlarm: true, alarm: true, offline: true, unavailable: true },
  });

  useEffect(() => {
    fetchLocalizationsData();
  }, []);

  useInterval(() => {
    fetchLocalizationsData();
  }, 3000);

  useDeepCompareEffect(() => {
    setFilteredLocalizatorData(filterLocalizatorData(filterParams, localizator.data));
  }, [localizator.data]);

  const handleFilter = (filteredObject: FilterValuesType) => {
    setFilterParams(filteredObject);
    setFilteredLocalizatorData(filterLocalizatorData(filteredObject, localizator.data));
  };

  return (
    <>
      {process.env.REACT_APP_GOOGLE_API_KEY ? (
        <Wrapper apiKey={process.env.REACT_APP_GOOGLE_API_KEY} render={() => <h1>Loading...</h1>}>
          <Map
            center={center}
            zoom={zoom}
            style={{ width: '100vw', height: '100vh' }}
            localizatorData={filteredLocalizatorData}
          >
            <Markers localizatorData={filteredLocalizatorData} />
          </Map>
        </Wrapper>
      ) : (
        console.error('Fill .env with google api key')
      )}
      <FilterModal>
        <FilterForm
          filterParams={filterParams}
          onHandleFilter={(filteredObject) => handleFilter(filteredObject)}
        />
      </FilterModal>
    </>
  );
};

const mapStateToProps = ({ localizator }: ILocalizator) => ({
  localizator,
});

const mapDispatchToProps = (
  dispatch: ThunkDispatch<ILocalizator, never, LocalizatorAllActionsTypes>,
) => ({
  fetchLocalizationsData: () => dispatch(fetchLocalizationsDataAction()),
});

const connector = connect(mapStateToProps, mapDispatchToProps);

export default connector(Localizator);
