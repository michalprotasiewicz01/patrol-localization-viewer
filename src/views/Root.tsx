import React from 'react';
import { Provider } from 'react-redux';
import store from 'store';
import CssBaseline from '@mui/material/CssBaseline';
import Localizator from './Localizator';

const Root = (): JSX.Element => (
  <Provider store={store}>
    <CssBaseline />
    <Localizator />
  </Provider>
);

export default Root;
