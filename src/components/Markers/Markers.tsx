import React, { useState, useEffect } from 'react';
import choseStatusMark from 'helper/choseStatusMark';
import generateHtmlListForInfoWindows from 'helper/generateHtmlListForInfoWindows';
import { ILocalizationObject } from 'models/localizatorRedux';
import { MarkerClusterer } from '@googlemaps/markerclusterer';

type MarkersProps = {
  localizatorData: ILocalizationObject[];
  map?: google.maps.Map;
};

const Markers = ({ localizatorData, ...props }: MarkersProps) => {
  const [markerCluster, setMarkerCluster] = useState<MarkerClusterer>();

  useEffect(() => {
    if (markerCluster) {
      markerCluster.clearMarkers();
    }

    const markersArray = localizatorData.map(
      ({
        sim_number,
        imei,
        last_latitude,
        last_longitude,
        last_altitude,
        last_speed,
        last_status,
        last_track_time,
        last_heartbeat,
      }) => {
        const marker = new google.maps.Marker({
          position: { lat: Number(last_latitude), lng: Number(last_longitude) },
          icon: choseStatusMark(last_status),
        });

        const infoWindow = new google.maps.InfoWindow({
          content: '',
          disableAutoPan: true,
        });

        marker.addListener('click', () => {
          infoWindow.setContent(
            generateHtmlListForInfoWindows(sim_number, [
              { label: 'sim number', value: sim_number },
              { label: 'imei', value: imei },
              { label: 'last latitude', value: last_latitude },
              { label: 'last longitude', value: last_longitude },
              { label: 'last altitude', value: last_altitude },
              { label: 'last speed', value: last_speed },
              { label: 'last status', value: last_status },
              { label: 'last track_time', value: last_track_time.toString() },
              { label: 'last heartbeat', value: last_heartbeat },
            ]),
          );
          infoWindow.open(props.map, marker);
        });

        return marker;
      },
    );

    setMarkerCluster(new MarkerClusterer({ map: props.map, markers: markersArray }));

    return () => {
      if (markerCluster) {
        markerCluster.clearMarkers();
      }
    };
  }, [props.map, localizatorData]);

  return null;
};

export default Markers;
