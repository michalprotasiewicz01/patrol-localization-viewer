import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import speedVariant from 'constants/speedVariant';
import FilterForm from './FilterForm';

const args = {
  filterParams: {
    phoneNumber: '',
    imei: '',
    speedVariant: [speedVariant.walk.text, speedVariant.bike.text, speedVariant.car.text],
    statuses: { noAlarm: true, alarm: true, offline: true, unavailable: true },
  },
};

export default {
  title: 'FilterForm',
  component: FilterForm,
  argTypes: {
    filterParams: { control: 'object' },
  },
} as ComponentMeta<typeof FilterForm>;

const Template: ComponentStory<typeof FilterForm> = (args) => <FilterForm {...args} />;

export const Base = Template.bind({});

Base.args = { ...args };
