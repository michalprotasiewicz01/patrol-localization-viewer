import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import FormControlLabel from '@mui/material/FormControlLabel';
import Input from '@mui/material/Input';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import DirectionsWalkIcon from '@mui/icons-material/DirectionsWalk';
import DirectionsBikeIcon from '@mui/icons-material/DirectionsBike';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import Typography from '@mui/material/Typography';

export type FilterValuesType = {
  phoneNumber: string;
  imei: string;
  speedVariant: string[];
  statuses: StatusesType;
};

type StatusesType = {
  noAlarm: boolean;
  alarm: boolean;
  offline: boolean;
  unavailable: boolean;
};

type FilterFormProps = {
  filterParams: FilterValuesType;
  onHandleFilter: (filterValues: FilterValuesType) => void;
};

const FilterForm = ({ filterParams, onHandleFilter }: FilterFormProps): JSX.Element => {
  const [{ phoneNumber, imei, speedVariant, statuses }, setFilterValues] =
    useState<FilterValuesType>(filterParams);

  useEffect(() => {
    setFilterValues(filterParams);
  }, [filterParams]);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    onHandleFilter({ phoneNumber, imei, speedVariant, statuses });
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit}
      sx={{
        display: 'flex',
        flexDirection: 'column',
        padding: '15px',
        width: '250px',
      }}
    >
      <Typography variant="h6" gutterBottom>
        Localize by:
      </Typography>
      <Input
        name="phoneNumber"
        fullWidth
        data-testid="phone-number-input"
        placeholder="Phone number"
        sx={{ mt: 1, mb: 2 }}
        value={phoneNumber}
        onChange={(event) =>
          setFilterValues((prevState) => ({ ...prevState, phoneNumber: event.target.value }))
        }
      />
      <Input
        name="imei"
        fullWidth
        data-testid="imei-input"
        placeholder="IMEI"
        sx={{ mt: 1, mb: 2 }}
        value={imei}
        onChange={(event) =>
          setFilterValues((prevState) => ({ ...prevState, imei: event.target.value }))
        }
      />
      <ToggleButtonGroup
        color="primary"
        sx={{
          alignSelf: 'center',
        }}
        value={speedVariant}
        onChange={(event, newValues: [string]) =>
          setFilterValues((prevState) => ({
            ...prevState,
            speedVariant: newValues,
          }))
        }
        aria-label="speed variant"
        data-testid="speed-variants-buttons"
      >
        <ToggleButton value="walk" aria-label="walk">
          <DirectionsWalkIcon />
        </ToggleButton>
        <ToggleButton value="bike" aria-label="bike">
          <DirectionsBikeIcon />
        </ToggleButton>
        <ToggleButton value="car" aria-label="car">
          <DirectionsCarIcon />
        </ToggleButton>
      </ToggleButtonGroup>
      <FormControlLabel
        control={<Checkbox value="noAlarm" color="primary" />}
        label="No Alarm"
        data-testid="no-alarm-checkbox"
        checked={statuses.noAlarm}
        onChange={(event, value) => {
          setFilterValues((prevState) => ({
            ...prevState,
            statuses: { ...prevState.statuses, noAlarm: value },
          }));
        }}
      />
      <FormControlLabel
        control={<Checkbox value="alarm" color="primary" />}
        label="Alarm"
        data-testid="alarm-checkbox"
        checked={statuses.alarm}
        onChange={(event, value) => {
          setFilterValues((prevState) => ({
            ...prevState,
            statuses: { ...prevState.statuses, alarm: value },
          }));
        }}
      />
      <FormControlLabel
        control={<Checkbox value="offline" color="primary" />}
        label="Device offline"
        data-testid="offline-checkbox"
        checked={statuses.offline}
        onChange={(event, value) => {
          setFilterValues((prevState) => ({
            ...prevState,
            statuses: { ...prevState.statuses, offline: value },
          }));
        }}
      />
      <FormControlLabel
        control={<Checkbox value="unavailable" color="primary" />}
        label="Device unavailable"
        data-testid="unavailable-checkbox"
        checked={statuses.unavailable}
        onChange={(event, value) => {
          setFilterValues((prevState) => ({
            ...prevState,
            statuses: { ...prevState.statuses, unavailable: value },
          }));
        }}
      />
      <Button
        type="submit"
        fullWidth
        variant="contained"
        sx={{ mt: 3, mb: 2 }}
        data-testid="submit-button"
      >
        Filter
      </Button>
    </Box>
  );
};

export default FilterForm;
