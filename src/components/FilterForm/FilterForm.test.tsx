import React from 'react';
import speedVariant from 'constants/speedVariant';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import FilterForm, { FilterValuesType } from './FilterForm';

const defaultProps = {
  filterParams: {
    phoneNumber: '',
    imei: '',
    speedVariant: [speedVariant.walk.text, speedVariant.bike.text, speedVariant.car.text],
    statuses: { noAlarm: true, alarm: true, offline: true, unavailable: true },
  },
  onHandleFilter: (filteredObject: FilterValuesType) => console.log(filteredObject),
};

describe('FilterForm', () => {
  it('should component correctly rendered', () => {
    render(<FilterForm {...defaultProps} />);
  });

  it('should change filtered object', async () => {
    const handleFilterTest = (filteredData: FilterValuesType) => {
      expect(filteredData.phoneNumber).toBe('+48 934');
      expect(filteredData.imei).toBe('164');
      expect(filteredData.speedVariant).toStrictEqual(['bike', 'car']);
      expect(filteredData.statuses).toStrictEqual({
        noAlarm: true,
        alarm: true,
        offline: false,
        unavailable: false,
      });
    };

    const user = userEvent.setup();
    const { getByTestId } = render(
      <FilterForm
        filterParams={defaultProps.filterParams}
        onHandleFilter={(filteredData) => handleFilterTest(filteredData)}
      />,
    );

    await user.type(getByTestId('phone-number-input'), '+48 934');
    await user.type(getByTestId('imei-input'), '164');
    await user.click(getByTestId('speed-variants-buttons').children[0]);
    await user.click(getByTestId('offline-checkbox'));
    await user.click(getByTestId('unavailable-checkbox'));

    await user.click(getByTestId('submit-button'));
  });
});
