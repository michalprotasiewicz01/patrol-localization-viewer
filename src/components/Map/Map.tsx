import React, { useState, useEffect, useRef } from 'react';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { ILocalizationObject } from 'models/localizatorRedux';

interface IMapProps extends google.maps.MapOptions {
  localizatorData: ILocalizationObject[];
  style: { [key: string]: string };
  onClick?: (e: google.maps.MapMouseEvent) => void;
  onIdle?: (map: google.maps.Map) => void;
  children?:
    | React.ReactElement<google.maps.MarkerOptions>[]
    | React.ReactElement<google.maps.MarkerOptions>;
}

const Map = ({
  localizatorData,
  onClick,
  onIdle,
  children,
  style,
  ...options
}: IMapProps): JSX.Element => {
  const ref = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<google.maps.Map>();

  useEffect(() => {
    if (ref.current && !map) {
      setMap(new window.google.maps.Map(ref.current, {}));
    }
  }, [ref, map]);

  useDeepCompareEffect(() => {
    if (map) {
      map.setOptions(options);
    }
  }, [map, options]);

  useEffect(() => {
    if (map) {
      ['click', 'idle'].forEach((eventName) => google.maps.event.clearListeners(map, eventName));

      if (onClick) {
        map.addListener('click', onClick);
      }

      if (onIdle) {
        map.addListener('idle', () => onIdle(map));
      }
    }
  }, [map, onClick, onIdle]);

  return (
    <>
      <div ref={ref} style={style} />
      {React.Children.map(
        children,
        (child) => React.isValidElement(child) && React.cloneElement(child, { map }),
      )}
    </>
  );
};

export default Map;
