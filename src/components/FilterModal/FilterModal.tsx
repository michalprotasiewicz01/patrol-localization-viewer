import React, { useState } from 'react';
import Fab from '@mui/material/Fab';
import MenuRoundedIcon from '@mui/icons-material/MenuRounded';
import Popover from '@mui/material/Popover';

export type FilterModalPropsType = {
  children: React.ReactNode;
};

const FilterModal = ({ children }: FilterModalPropsType): JSX.Element => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <>
      <Fab
        color="primary"
        sx={{ position: 'absolute', bottom: '15px', left: '15px' }}
        aria-label="filers options"
        onClick={handleClick}
        data-testid="open-filter-options"
      >
        <MenuRoundedIcon />
      </Fab>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        data-testid="filter-options"
      >
        {children}
      </Popover>
    </>
  );
};

export default FilterModal;
