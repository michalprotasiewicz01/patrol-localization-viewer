import React from 'react';
import { render } from '@testing-library/react';
import FilterModal from './FilterModal';
import userEvent from '@testing-library/user-event';

describe('FilterModal', () => {
  it('should component correctly rendered', () => {
    render(
      <FilterModal>
        <h1>FilterModal</h1>
      </FilterModal>,
    );
  });

  it('should show content when clicked', async () => {
    const user = userEvent.setup();

    const { getByTestId } = render(
      <FilterModal>
        <h1>FilterModal</h1>
      </FilterModal>,
    );

    await user.click(getByTestId('open-filter-options'));
    expect(getByTestId('filter-options')).toHaveTextContent('FilterModal');
  });
});
