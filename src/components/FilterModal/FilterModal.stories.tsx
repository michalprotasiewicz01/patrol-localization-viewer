import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import FilterModal from './FilterModal';

export default {
  title: 'FilterModal',
  component: FilterModal,
} as ComponentMeta<typeof FilterModal>;

const Template: ComponentStory<typeof FilterModal> = (args) => (
  <FilterModal {...args}>
    <h1>Filter modal</h1>
  </FilterModal>
);

export const Base = Template.bind({});

Base.args = {};
