# Localizator <img alt="icon logo" height="45" src="https://www.svgrepo.com/show/55966/radar.svg">
Localizator is an application for locating a group of devices.

## Table of contents <img alt="icon table-of-contents" height="30" src="https://www.svgrepo.com/show/336182/table.svg">
- [Localizator](#localizator)
  - [Main technologies](#main-technologies)
  - [Installation](#installation)
  - [Useful commands](#useful-commands)

## Main technologies <img alt="icon technologies" height="30" src="https://www.svgrepo.com/show/164928/networking.svg">
- HTML <img alt="icon html" height="20" src="https://www.svgrepo.com/show/349402/html5.svg">
- CSS <img alt="icon css" height="20" src="https://www.svgrepo.com/show/373535/css.svg">
- TypeScript <img alt="icon Typescript" height="20" src="https://www.svgrepo.com/show/349540/typescript.svg">
- React <img alt="icon react" height="20" src="https://www.svgrepo.com/show/354259/react.svg">
- Redux <img alt="icon redux" height="20" src="https://www.svgrepo.com/show/354274/redux.svg">
- axios <img alt="icon axios" height="20" src="https://user-images.githubusercontent.com/8939680/57233884-20344080-6fe5-11e9-8df3-0df1282e1574.png">
- React Testing Library <img alt="icon ReactTestingLibrary" height="20" src="https://www.svgrepo.com/show/354264/react-styleguidist.svg">
- Storybook <img alt="icon Storybook" height="20" src="https://www.svgrepo.com/show/354397/storybook-icon.svg">

## Installation <img alt="icon instalation" height="30" src="https://www.svgrepo.com/show/387853/install.svg">
- download source code
- open in terminal main directory of source project
- copy `.env.example` file to `.env` and fill variable with your google api key
- install node.js (v16 or later) https://nodejs.org/en/download/
- `yarn install` or `npm install`
- `yarn start` or `npm start`

## Useful commands <img alt="icon commands" height="30" src="https://www.svgrepo.com/show/336126/terminal.svg">
- `yarn start` - start app
- `yarn build`- build app
- `yarn test` - run unit tests
- `yarn storybook` - run storybook
- `yarn build-storybook` - build storybook
